"""
Instructor implementation for Problem 1 of Homework 0, ESE 5460.

Author(s):
    Michael Yao, michael [dot] yao [at] pennmedicine [dot] upenn [dot] edu

Licensed under the MIT License. Copyright University of Pennsylvania 2023.
"""
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
from typing import Optional, Union


class ProblemOneFunction:
    """Defines the function from Problem 1."""

    x1_bounds: np.ndarray = np.array([-3.0, 3.0])

    x2_bounds: np.ndarray = np.array([-3.0, 3.0])

    def __call__(
        self,
        x1: Union[float, int, np.ndarray],
        x2: Union[float, int, np.ndarray]
    ) -> Union[float, np.ndarray]:
        """
        Implementation of the multivariate function given in the problem.
        Input:
            x1: the variable x1 as either a scalar or an array of x1 values.
            x2: the variable x2 as either a scalar or an array of x2 values.
        Returns:
            The function value f(x1, x2).
        """
        self.validate_input(x1, x2)
        return (2.0 * x1 ** 2) - (1.05 * x1 ** 4) + (
            (x1 ** 6 / 6.0) - (x1 * x2) + (x2 ** 2)
        )

    def validate_input(
        self,
        x1: Union[float, int, np.ndarray],
        x2: Union[float, int, np.ndarray]
    ) -> None:
        """
        Validates the inputs into the function.
        Input:
            x1: the variable x1 as either a scalar or an array of x1 values.
            x2: the variable x2 as either a scalar or an array of x2 values.
        Returns:
            None.
        """
        x1min, x1max = self.x1_bounds
        x2min, x2max = self.x2_bounds
        if not np.all(x1 >= x1min) or not np.all(x1 <= x1max):
            raise ValueError(
                f"`x1` must be between {x1min} and {x1max}, got {x1}"
            )
        if not np.all(x2 >= x2min) or not np.all(x2 <= x2max):
            raise ValueError(
                f"`x2` must be between {x2min} and {x2max}, got {x2}"
            )


def main(
    savepath: Optional[Union[Path, str]] = None, cmap: str = "viridis"
) -> None:
    """
    Plots the contour plot of f() in the specified region.
    Input:
        savepath: path to save the contour plot to. Default not saved.
        cmap: color map for contour plot. Default viridis.
    Returns:
        None.
    """
    f = ProblemOneFunction()

    x1min, x1max = f.x1_bounds
    x2min, x2max = f.x2_bounds
    x1 = np.linspace(x1min, x1max, num=100)
    x2 = np.linspace(x2min, x2max, num=100)

    X1, X2 = np.meshgrid(x1, x2)

    plt.figure()
    plt.contourf(X1, X2, f(X1, X2), cmap=cmap, levels=100)
    plt.title(r"Contour Plot of f($x_1$, $x_2$)")
    plt.xlabel(r"$x_1$")
    plt.ylabel(r"$x_2$")
    plt.colorbar()

    if savepath:
        plt.savefig(savepath, dpi=600, bbox_inches="tight", transparent=True)
    else:
        plt.show()
    plt.close()


if __name__ == "__main__":
    main()
