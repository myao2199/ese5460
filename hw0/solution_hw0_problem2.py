"""
Instructor implementation for Problem 2 of Homework 0, ESE 5460.

Author(s):
    Michael Yao, michael [dot] yao [at] pennmedicine [dot] upenn [dot] edu

Licensed under the MIT License. Copyright University of Pennsylvania 2023.
"""
import numpy as np
import scipy.optimize as optim
from functools import partial
from typing import Any, Dict


def f(X: np.ndarray) -> float:
    """
    Loss function implementation based on the loss function f(x, y) given
    in the problem.
    Input:
        X: a matrix of input values [x, y].
    Returns:
        The value of the loss function at [x, y].
    """
    x, y = X
    return (x * x) + (y * y) - (6.0 * x * y) - (4.0 * x) - (5.0 * y)


def g1(X: np.ndarray, perturb: bool = False) -> np.ndarray:
    """
    The first constraint in the problem y <= -(x - 2)^2 + 4 rewritten as a
    function g1(X) <= 0.
    Input:
        X: a matrix of input values [x, y].
        perturb: whether to perturb the first constraint in the manner stated
            in Part (b) of the problem.
    Returns:
        The value of the constraint function g1(X) at X = [x, y].
    """
    x, y = X
    return y + ((x - 2) * (x - 2)) - 4.0 - (float(perturb) * 0.1)


def g2(X: np.ndarray) -> np.ndarray:
    """
    The first constraint in the problem y >= -x + 1 rewritten as a function
    g2(X) <= 0.
    Input:
        X: a matrix of input values [x, y].
    Returns:
        The value of the constraint function g2(X) at X = [x, y].
    """
    x, y = X
    return -1.0 * (x + y - 1.0)


def p3a():
    """
    Code to help execute numerical computational steps for Problem 3(a).
    Input:
        None.
    Returns:
        None.
    """
    coord = [
        np.array([4.79129, -3.79129]),
        np.array([2.69623, 3.51526]),
        np.array([0.4375, 0.5625]),
        np.array([0.208712, 0.791288])
    ]
    loc, minimum = None, np.inf
    for X in coord:
        fX = f(X)
        loc, minimum = (X, fX) if fX < minimum else (loc, minimum)
    print(f" fun: {minimum}\n   x: {loc}\n")


def main(seed: int = 5460, perturb: bool = False) -> Dict[str, Any]:
    """
    Problem 2 implementation.
    Input:
        seed: random seed. Default 5460.
        perturb: whether to perturb the first constraint in the manner stated
            in Part (b) of the problem.
    Returns:
        A dictionary containing the optimization results.
    """
    ndim: int = 2

    x0: np.ndarray = np.random.default_rng(seed=seed).normal(size=ndim)

    constraints = [
        optim.NonlinearConstraint(
            fun=partial(g1, perturb=perturb), lb=-np.inf, ub=0.0
        ),
        optim.NonlinearConstraint(fun=g2, lb=-np.inf, ub=0.0)
    ]

    optimization_results = optim.minimize(
        f, x0, constraints=constraints
    )
    return dict(optimization_results)


if __name__ == "__main__":
    main(perturb=False)
    main(perturb=True)
