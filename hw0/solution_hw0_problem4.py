"""
Instructor implementation for Problem 4 of Homework 0, ESE 5460.

Author(s):
    Michael Yao, michael [dot] yao [at] pennmedicine [dot] upenn [dot] edu

Licensed under the MIT License. Copyright University of Pennsylvania 2024.
"""
import numpy as np
import pandas as pd
from typing import Dict, Generator, Optional, Sequence, Tuple, Union


def load_and_split_dataset(
    dataset_url: str = (
        "https://raw.githubusercontent.com/selva86/datasets/master/"
        "BostonHousing.csv"
    ),
    target_label: str = "medv",
    val_frac: float = 0.2,
    seed: Optional[Union[int, Generator]] = None
) -> Tuple[np.ndarray]:
    """
    Loads the Boston housing dataset and splits it into training and
    validation partitions.
    Input:
        dataset_url: the URL (or file path) to laod the dataset from.
        target_label: the target label.
        val_frac: fraction of the dataset to use for linear regression model
            validation. Default 20%.
        seed: optional random seed or generator. Default None.
    Returns:
        X_train: the input training covariates.
        y_train: the output training values.
        X_val: the input validation covariates.
        y_val: the output validation values.
    """
    # Read the Boston housing dataset.
    dataset = pd.read_csv(dataset_url)

    # Initialize the random number generator to shuffle the dataset.
    rng = seed
    if seed is None or not isinstance(seed, Generator):
        rng = np.random.default_rng(seed=seed)

    # Shuffle the rows of the dataset.
    dataset = dataset.sample(frac=1, random_state=rng).reset_index()

    # Separate the dataset into inputs and outputs.
    X = dataset.drop(columns=target_label).to_numpy()
    y = dataset[target_label].to_numpy()

    # Add the feature corresponding to the bias term to the dataset.
    X = np.concatenate((np.ones_like(y)[..., np.newaxis], X), axis=-1)

    # Split the dataset into training and validation partitions.
    num_val = round(min(max(val_frac, 0.0), 1.0) * X.shape[0])
    X_train, y_train = X[num_val:, :], y[num_val:]
    X_val, y_val = X[:num_val, :], y[:num_val]
    return X_train, y_train, X_val, y_val


def loss(
    X: np.ndarray, y: np.ndarray, w: np.ndarray, b: float
) -> np.ndarray:
    """
    Implementation of the loss function ell(w, b) given in the problem.
    Input:
        X: data matrix with dimensions NxD.
        Y: target vector with dimensions N.
        w: weight vector with dimensions D.
        b: bias term as a float.
    Returns:
        The value of the loss function given X, Y, w, and b.
    """
    w = np.concatenate(([b], w))
    return np.square(np.linalg.norm(y - (X @ w))) / (2.0 * X.shape[0])


def main(
    val_frac: float = 0.2,
    num_reps: int = 3,
    seed: int = 5460
) -> Dict[str, Sequence[float]]:
    """
    Problem 4 implementation.
    Input:
        val_frac: fraction of the dataset to use for linear regression model
            validation. Default 20%.
        num_reps: number of replications of the experiment to run. Default 3.
        seed: random seed. Default 5460.
    Returns:
        A dictionary containing the experimental results.
    """
    train_loss, val_loss = [], []
    for i in range(num_reps):
        # Load the Boston housing dataset.
        X_train, y_train, X_val, y_val = load_and_split_dataset(
            seed=(seed + i), val_frac=val_frac
        )

        # Fit the linear regression model and solve for w*, b*.
        W = np.linalg.inv(X_train.T @ X_train) @ X_train.T @ y_train
        w_star, b_star = W[1:], W[0]

        # Calculate the training and validation losses.
        train_loss.append(loss(X_train, y_train, w_star, b_star))
        val_loss.append(loss(X_val, y_val, w_star, b_star))

    # Report experimental results.
    return {"train_loss": train_loss, "val_loss": val_loss}


if __name__ == "__main__":
    main()
